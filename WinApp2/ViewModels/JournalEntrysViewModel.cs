﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using WinApp2.Models;

namespace WinApp2.ViewModels
{
    public class JournalEntrysViewModel : ViewModelBase
    {
        private ObservableCollection<JournalEntryViewModel> _journalEntrys;

        public ObservableCollection<JournalEntryViewModel> JournalEntrys
        {
            get { return _journalEntrys;}
            set
            {
                _journalEntrys = value;
                RaisePropertyChanged("JournalEntrys");
            }
        }

        private App app = (Application.Current as App);

        public ObservableCollection<JournalEntryViewModel> GetJournalEntrys()
        {
            _journalEntrys = new ObservableCollection<JournalEntryViewModel>();
            using (var db = new SQLite.SQLiteConnection(app.DBPath))
            {
                var items = db.Table<JournalEntry>()
                    .OrderBy(c => c.Id)
                    .Select(j => new JournalEntryViewModel()
                    {
                        Id = j.Id, 
                        Title = j.Title,
                        Content = j.Content
                    });
                foreach (var item in items)
                {
                    _journalEntrys.Add(item);
                }
            }
            return _journalEntrys;
        } 
    }
}
