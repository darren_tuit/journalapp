﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using WinApp2.ViewModels;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace WinApp2.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private JournalEntrysViewModel _journalEntrysViewModel = null;
        private ObservableCollection<JournalEntryViewModel> journalEntrys = null; 
        
        public MainPage()
        {

            DataContext = this;

            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            _journalEntrysViewModel = new JournalEntrysViewModel();
            journalEntrys = _journalEntrysViewModel.GetJournalEntrys();

            journalEntryListView.ItemsSource = journalEntrys;
            base.OnNavigatedTo(e);
        }

        //public void testAddItems()
        //{
        //   journalEntrys.Add(new JournalEntryViewModel()
        //   {
        //       Id = 2,
        //       Title = "A New Journal Entry",
        //       Content = "Journal Entry Content blah blah"
        //   });
        //}
    }
}
