﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using WinApp2.Models;

namespace WinApp2
{
    public static class ResetData
    {
        public static void ResetDBData()
        {
            var app = (Application.Current as App);
            using (var db = new SQLite.SQLiteConnection(app.DBPath))
            {
                db.DeleteAll<JournalEntry>();

                db.Insert(new JournalEntry()
                {
                    Id = 1,
                    Title = "Journal Entry 1",
                    Content = "A journal entry content string blah blah"
                });

                db.Insert(new JournalEntry()
                {
                    Id = 2,
                    Title = "Journal Entry 2",
                    Content = "A journal entry content string 2 blah blah"
                });
            }
        }
    }
}
